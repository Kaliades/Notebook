# Notebook 

App is in [Google play store](https://play.google.com/store/apps/details?id=com.szymonkulinski.simplenoteapplication)

With this application on the Android platform you can create notes and save them in the database. 

### Technologies:
* Programming language: __Kotiln__
* Architecture pattern: __MVVM__
* Frameworks used:  
    * __Dagger 2__ 
    * __Room__ 
    * __LiveData__ 
### Inspiration
This application has been inspired by [RecyclerViewTutorial2017](https://github.com/BracketCove/RoomDemo2017).