package com.szymonkulinski.simplenoteapplication.data.room_db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.szymonkulinski.simplenoteapplication.data.room_db.model.Note

@Dao
interface RoomDao {

    @Query("SELECT * FROM Note")
    fun getListOfNotes(): LiveData<List<Note>>

    @Query("SELECT * FROM Note WHERE id = :id")
    fun getNoteById(id: Int): LiveData<Note>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun inserOrUpdateNote(note: Note)

    @Delete
    fun delteNote(note: Note)
}