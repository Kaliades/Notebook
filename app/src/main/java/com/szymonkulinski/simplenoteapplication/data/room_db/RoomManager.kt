package com.szymonkulinski.simplenoteapplication.data.room_db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.szymonkulinski.simplenoteapplication.data.room_db.model.Note

@Database(entities = [Note::class], version = 1, exportSchema = false)
abstract class RoomManager : RoomDatabase() {

    abstract fun roomDao(): RoomDao
}