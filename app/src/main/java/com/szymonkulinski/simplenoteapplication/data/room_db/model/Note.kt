package com.szymonkulinski.simplenoteapplication.data.room_db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Note(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val title: String,
    val value: String,
    val data: String
)