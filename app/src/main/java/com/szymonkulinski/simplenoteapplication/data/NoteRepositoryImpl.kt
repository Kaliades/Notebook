package com.szymonkulinski.simplenoteapplication.data

import androidx.lifecycle.LiveData
import com.szymonkulinski.simplenoteapplication.data.room_db.RoomDao
import com.szymonkulinski.simplenoteapplication.data.room_db.model.Note
import com.szymonkulinski.simplenoteapplication.domain.repository.NoteRepository
import javax.inject.Inject

class NoteRepositoryImpl @Inject constructor(private val roomDao: RoomDao) : NoteRepository {

    override fun getListOfNotes(): LiveData<List<Note>> {
        return roomDao.getListOfNotes()
    }

    override fun getNoteById(id: Int): LiveData<Note> {
        return roomDao.getNoteById(id)
    }

    override fun insertOrUpdateNote(note: Note) {
        roomDao.inserOrUpdateNote(note)
    }

    override fun deleteNote(note: Note) {
        roomDao.delteNote(note)
    }
}