package com.szymonkulinski.simplenoteapplication.ui.about

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.szymonkulinski.simplenoteapplication.NoteApplication
import com.szymonkulinski.simplenoteapplication.R
import com.szymonkulinski.simplenoteapplication.data.room_db.model.Note
import com.szymonkulinski.simplenoteapplication.domain.viewmodels.FactoryOfNoteViewModel
import com.szymonkulinski.simplenoteapplication.domain.viewmodels.NoteViewModel
import kotlinx.android.synthetic.main.fragment_about.*
import javax.inject.Inject

class AboutFragment : Fragment() {

    @Inject
    lateinit var factory: FactoryOfNoteViewModel
    private lateinit var noteViewModel: NoteViewModel
    private var idNote = -1
    private lateinit var note: Note
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity?.application as NoteApplication).applicationComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()
        getIdFromBundle()
        setHasOptionsMenu(true)
        getNoteViewMode()
        getNote()
    }

    private fun getIdFromBundle() {
        idNote = arguments?.getInt(ID_KEY) ?: -1
    }

    private fun getNoteViewMode() {
        noteViewModel = ViewModelProviders.of(this, factory).get(NoteViewModel::class.java)
    }

    private fun getNote() {
        noteViewModel.getNoteById(idNote).observe(viewLifecycleOwner, Observer {
            setNoteValue(it)
            note = it
        })
    }

    private fun setNoteValue(note: Note) {
        fragment_about_title.text = note.title
        fragment_about_data.text = note.data
        fragment_about_note.text = note.value
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.menu_about, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_delete) {
            showAlert()
            return true
        }
        return false
    }

    private fun showAlert() {
        AlertDialog.Builder(context)
            .setMessage(R.string.alert_string_delete)
            .setPositiveButton(
                R.string.alert_ok
            ) { _: DialogInterface, _: Int -> deleteNoteAndFinishThisFragment() }
            .setNegativeButton(R.string.alert_cancel, null)
            .create()
            .show()
    }

    private fun deleteNoteAndFinishThisFragment() {
        noteViewModel.deleteNote(note)
        navController.navigateUp()
    }

    companion object {
        const val ID_KEY = "ID_KEY"
    }

}



























