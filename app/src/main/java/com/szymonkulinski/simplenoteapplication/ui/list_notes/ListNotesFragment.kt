package com.szymonkulinski.simplenoteapplication.ui.list_notes

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.szymonkulinski.simplenoteapplication.NoteApplication
import com.szymonkulinski.simplenoteapplication.R
import com.szymonkulinski.simplenoteapplication.data.room_db.model.Note
import com.szymonkulinski.simplenoteapplication.domain.viewmodels.FactoryOfNoteViewModel
import com.szymonkulinski.simplenoteapplication.domain.viewmodels.NotesCollectionViewModel
import kotlinx.android.synthetic.main.fragment_list_of_notes.*
import javax.inject.Inject

class ListNotesFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: FactoryOfNoteViewModel
    private lateinit var notesCollections: NotesCollectionViewModel

    private var listOfNotes: List<Note> = emptyList()
    private lateinit var adapter: ListNotesAdapter

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity?.application as NoteApplication).applicationComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list_of_notes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()
        list_notes_fab.setOnClickListener { startCreateFragment() }
        setRecyclerView()
        setHasOptionsMenu(true)
        getNotesCollections()
        observeNotesCollections()
    }

    private fun getNotesCollections() {
        notesCollections = ViewModelProviders.of(this, viewModelFactory)
            .get(NotesCollectionViewModel::class.java)
    }

    private fun observeNotesCollections() {
        notesCollections.getListOfNotes().observe(viewLifecycleOwner,
            Observer<List<Note>> {
                adapter.setNewList(it)
                listOfNotes = it
            })
    }

    private fun setRecyclerView() {
        adapter = ListNotesAdapter(navController)
        list_notes_list.layoutManager = LinearLayoutManager(context)
        list_notes_list.adapter = adapter
    }

    private fun startCreateFragment() {
        navController.navigate(R.id.action_listNotesFragment_to_createFragment)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.menu_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_delete_all) {
            if (listOfNotes.isNotEmpty())
                showAlertDialog()
            return true
        }
        return false
    }

    private fun showAlertDialog() {
        val alert = AlertDialog.Builder(context)
        alert.setMessage(R.string.alert_string_delete_all)
        alert.setPositiveButton(
            R.string.alert_ok
        ) { _: DialogInterface, _: Int -> notesCollections.delteNote(listOfNotes) }
        alert.setNegativeButton(R.string.alert_cancel, null)
        alert.create().show()
    }
}