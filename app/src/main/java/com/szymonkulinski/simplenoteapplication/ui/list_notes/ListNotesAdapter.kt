package com.szymonkulinski.simplenoteapplication.ui.list_notes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.szymonkulinski.simplenoteapplication.R
import com.szymonkulinski.simplenoteapplication.data.room_db.model.Note
import com.szymonkulinski.simplenoteapplication.ui.about.AboutFragment
import kotlinx.android.synthetic.main.notes_list_item.view.*

class ListNotesAdapter(private val navController: NavController) :
    RecyclerView.Adapter<ListNotesAdapter.ViewHolder>() {

    private var listNotes: List<Note> = emptyList()

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        val title = view.list_item_text!!
        val data = view.list_item_data!!
        private val layout = view.list_item_layout!!

        init {
            layout.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val id = listNotes[adapterPosition].id
            navController.navigate(
                R.id.action_listNotesFragment_to_aboutFragment,
                bundleOf(Pair(AboutFragment.ID_KEY, id))
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.notes_list_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return listNotes.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var text = listNotes[position].title
        if (text.isBlank())
            text = listNotes[position].value
        holder.title.text = text
        holder.data.text = listNotes[position].data
    }

    internal fun setNewList(list: List<Note>) {
        listNotes = list
        this.notifyDataSetChanged()
    }

}