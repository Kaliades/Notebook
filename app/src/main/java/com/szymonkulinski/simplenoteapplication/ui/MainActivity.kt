package com.szymonkulinski.simplenoteapplication.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.szymonkulinski.simplenoteapplication.R
import com.szymonkulinski.simplenoteapplication.ui.list_notes.ListNotesFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
    }

}
