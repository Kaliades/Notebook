package com.szymonkulinski.simplenoteapplication.ui.create

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.szymonkulinski.simplenoteapplication.NoteApplication
import com.szymonkulinski.simplenoteapplication.R
import com.szymonkulinski.simplenoteapplication.data.room_db.model.Note
import com.szymonkulinski.simplenoteapplication.domain.util.DataHelper
import com.szymonkulinski.simplenoteapplication.domain.viewmodels.FactoryOfNoteViewModel
import com.szymonkulinski.simplenoteapplication.domain.viewmodels.NewNoteViewModel
import kotlinx.android.synthetic.main.fragment_create.*
import javax.inject.Inject

class CreateFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: FactoryOfNoteViewModel
    lateinit var newNoteViewModel: NewNoteViewModel

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity?.application as NoteApplication).applicationComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_create, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()
        setHasOptionsMenu(true)
        getNewNoteViewModel()
    }

    private fun getNewNoteViewModel() {
        newNoteViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(NewNoteViewModel::class.java)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.menu_create, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_cancel_to_db -> {
                navController.navigateUp()
                true
            }
            R.id.action_accept_to_db -> {
                if (!insertToDb())
                    showAlert()
                else navController.navigateUp()
                true
            }
            else -> false
        }
    }

    private fun showAlert() {
        AlertDialog.Builder(context)
            .setMessage(R.string.alert_empty_title_or_note)
            .create()
            .show()
    }

    private fun insertToDb(): Boolean {
        val title = fragment_create_title.text.toString()
        val note = fragment_create_note.text.toString()
        if (title.isNotBlank() || note.isNotBlank()) {
            newNoteViewModel.insertOrUpdateNote(Note(0, title, note, DataHelper.getCurrentData()))
            return true
        }
        return false
    }
}