package com.szymonkulinski.simplenoteapplication.domain.viewmodels

import android.annotation.SuppressLint
import android.os.AsyncTask
import androidx.lifecycle.ViewModel
import com.szymonkulinski.simplenoteapplication.data.room_db.model.Note
import com.szymonkulinski.simplenoteapplication.domain.repository.NoteRepository

class NewNoteViewModel(private val repository: NoteRepository) : ViewModel() {

    fun insertOrUpdateNote(note: Note) = Task().execute(note)!!

    @SuppressLint("StaticFieldLeak")
    private inner class Task : AsyncTask<Note, Unit, Unit>() {
        override fun doInBackground(vararg params: Note?) {
            repository.insertOrUpdateNote(params[0]!!)
        }
    }
}