package com.szymonkulinski.simplenoteapplication.domain.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.szymonkulinski.simplenoteapplication.domain.repository.NoteRepository
import javax.inject.Inject

class FactoryOfNoteViewModel @Inject constructor(private val repository: NoteRepository) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NotesCollectionViewModel::class.java))
            return NotesCollectionViewModel(repository) as T
        if (modelClass.isAssignableFrom(NoteViewModel::class.java))
            return NoteViewModel(repository) as T
        if (modelClass.isAssignableFrom(NewNoteViewModel::class.java))
            return NewNoteViewModel(repository) as T
        throw IllegalArgumentException("ViewModel not fund !")
    }
}