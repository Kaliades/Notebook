package com.szymonkulinski.simplenoteapplication.domain.repository

import androidx.lifecycle.LiveData
import com.szymonkulinski.simplenoteapplication.data.room_db.model.Note

interface NoteRepository {

    fun getListOfNotes(): LiveData<List<Note>>

    fun getNoteById(id: Int): LiveData<Note>

    fun insertOrUpdateNote(note: Note)

    fun deleteNote(note: Note)


}