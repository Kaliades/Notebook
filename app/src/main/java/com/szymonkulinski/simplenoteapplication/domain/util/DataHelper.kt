package com.szymonkulinski.simplenoteapplication.domain.util

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

object DataHelper {

    @SuppressLint("SimpleDateFormat")
    fun getCurrentData(): String {
        val format = SimpleDateFormat("dd/MM/yyyy")
        return format.format(Calendar.getInstance().time)
    }
}