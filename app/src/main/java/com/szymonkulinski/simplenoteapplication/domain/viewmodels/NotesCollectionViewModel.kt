package com.szymonkulinski.simplenoteapplication.domain.viewmodels

import android.annotation.SuppressLint
import android.os.AsyncTask
import androidx.lifecycle.ViewModel
import com.szymonkulinski.simplenoteapplication.data.room_db.model.Note
import com.szymonkulinski.simplenoteapplication.domain.repository.NoteRepository

class NotesCollectionViewModel(private val repository: NoteRepository) : ViewModel() {

    fun getListOfNotes() = repository.getListOfNotes()

    fun delteNote(notes: List<Note>) = Task().execute(notes)

    @SuppressLint("StaticFieldLeak")
    private inner class Task : AsyncTask<List<Note>, Unit, Unit>() {
        override fun doInBackground(vararg params: List<Note>?) {
            params[0]!!.forEach { repository.deleteNote(it) }
        }
    }


}