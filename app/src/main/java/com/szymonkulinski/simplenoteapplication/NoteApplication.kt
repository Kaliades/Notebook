package com.szymonkulinski.simplenoteapplication

import android.app.Application
import com.szymonkulinski.simplenoteapplication.di.ApplicationComponent
import com.szymonkulinski.simplenoteapplication.di.ApplicationModule
import com.szymonkulinski.simplenoteapplication.di.DaggerApplicationComponent
import com.szymonkulinski.simplenoteapplication.di.RoomModule


class NoteApplication : Application() {

    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        applicationComponent = DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .roomModule(RoomModule(this))
            .build()


    }
}