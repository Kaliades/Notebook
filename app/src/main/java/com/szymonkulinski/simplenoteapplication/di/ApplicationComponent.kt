package com.szymonkulinski.simplenoteapplication.di

import android.app.Application
import com.szymonkulinski.simplenoteapplication.ui.about.AboutFragment
import com.szymonkulinski.simplenoteapplication.ui.create.CreateFragment
import com.szymonkulinski.simplenoteapplication.ui.list_notes.ListNotesFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RoomModule::class, ApplicationModule::class])
interface ApplicationComponent {

    fun inject(createFragment: CreateFragment)
    fun inject(listNotesFragment: ListNotesFragment)
    fun inject(aboutFragment: AboutFragment)
    fun application():Application
}