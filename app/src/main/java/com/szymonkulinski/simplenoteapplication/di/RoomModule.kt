package com.szymonkulinski.simplenoteapplication.di

import android.app.Application
import androidx.room.Room
import com.szymonkulinski.simplenoteapplication.data.NoteRepositoryImpl
import com.szymonkulinski.simplenoteapplication.data.room_db.RoomDao
import com.szymonkulinski.simplenoteapplication.data.room_db.RoomManager
import com.szymonkulinski.simplenoteapplication.domain.repository.NoteRepository
import com.szymonkulinski.simplenoteapplication.domain.viewmodels.FactoryOfNoteViewModel
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule(application: Application) {

    private val roomDb = Room.databaseBuilder(
        application,
        RoomManager::class.java,
        "Note.db"
    ).build()

    @Singleton
    @Provides
    fun provideRoomDao(roomDataBase: RoomManager) = roomDataBase.roomDao()

    @Singleton
    @Provides
    fun provideRepository(roomDao: RoomDao): NoteRepository = NoteRepositoryImpl(roomDao)

    @Singleton
    @Provides
    fun provideDataBase() = roomDb

    @Singleton
    @Provides
    fun provideFactoryViewModel(repository: NoteRepository) = FactoryOfNoteViewModel(repository)

}