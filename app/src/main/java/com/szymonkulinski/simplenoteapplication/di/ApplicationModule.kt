package com.szymonkulinski.simplenoteapplication.di

import android.app.Application
import com.szymonkulinski.simplenoteapplication.NoteApplication
import dagger.Module
import dagger.Provides


@Module
class ApplicationModule(private val noteApplication: NoteApplication) {

    @Provides
    fun provideApplication(): Application = noteApplication


}